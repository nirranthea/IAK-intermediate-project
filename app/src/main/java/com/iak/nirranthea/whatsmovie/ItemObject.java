package com.iak.nirranthea.whatsmovie;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zidi on 10/02/2018.
 */

//TODO: 9 Buat Object dari variabel yang akan kita gunakan untuk diSet di Android
public class ItemObject {

    @SerializedName("results")
    List<Results> results;

    public class Results {
        @SerializedName("original_title")
        public String original_title;

        @SerializedName("overview")
        public String overview;

        @SerializedName("release_date")
        public String release_date;

        @SerializedName("backdrop_path")
        public String backdrop_path;

        @SerializedName("vote_average")
        public String vote_average;

        @SerializedName("poster_path")
        public String poster_path;

        @SerializedName("id")
        public String id;
    }
}
