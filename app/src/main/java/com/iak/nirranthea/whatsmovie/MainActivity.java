package com.iak.nirranthea.whatsmovie;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.design.widget.Snackbar;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iak.nirranthea.whatsmovie.adapter.RecyclerViewAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class MainActivity extends AppCompatActivity {

    //TODO: 14 Deklarasi Variable
    private final String API_KEY = "e052653fbd2bce9cacba98622d637e8c";
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private RecyclerViewAdapter recyclerViewAdapter;
    private String FilmCategory;
    private ProgressDialog dialog;
    private AlertDialog.Builder alert;
    private ItemObject a;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String movieCategory;

    private static final int TIME_LIMIT = 1800;
    private static long backPressed;

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        //TODO : tambah SwipeRefresh
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_orange_dark);

        onSwipeRefresh();
    }

    private void onSwipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh(){
                initViews();
            }
        });
    }

    private void initViews() {
        //TODO: 17 Deklarasi dan Inisialisasi Komponen
        alert = new AlertDialog.Builder(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//        layoutManager = new GridLayoutManager(MainActivity.this,2);
        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(layoutManager);

        // check screen orientation
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // if portrait
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            // if landscape
            recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        }

        //TODO: 18b Panggil Method
        requestJsonObject(1);
    }

    //TODO: 18 Buat Method untuk tentukan URL Request
    private void requestJsonObject(int i){
        //Kondisional untuk tentukan Kategori Film yg akan ditampilkan
        switch (i){
            case 0:
                //setTitle("Popular Movie");
                movieCategory = "Popular Movies";
                FilmCategory="popular";
                break;
            case 1:
//                setTitle("Now Playing Movie");
                movieCategory = "Now Playing Movies";
                FilmCategory="now_playing";
                break;
            case 2:
//                setTitle("Top Rated Movie");
                movieCategory = "Top Rated Movies";
                FilmCategory="top_rated";
                break;
            case 3:
//                setTitle("Coming Soon");
                movieCategory = "Coming Soon Movies";
                FilmCategory="upcoming";
                break;
            default:
                break;
        }
        setTitle(movieCategory);

        //BUILD URL
        RequestParams params = new RequestParams();
        params.put("api_key",API_KEY);
        String FullURL = "https://api.themoviedb.org/3/movie/" + FilmCategory;

        //Panggil URL melalui Method MyParsingGson
        MyParsingGson(params,FullURL);

    }

    //TODO 19: buat Method untuk Build dan Request JSON
    private void MyParsingGson(RequestParams params,String url){

        //Inisialisasi dan panggil Kotak Dialog
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Loading Movies, Please Wait . . .");
        dialog.setCancelable(true);
        dialog.show();

        //TODO 20:
        //Deklarasi Async
        AsyncHttpClient client = new AsyncHttpClient();

        //Async nembak URL
        client.get(url, params, new AsyncHttpResponseHandler(){

            //jika diterima
            @Override
            public void onSuccess(String response){
                //sembunyikan kotak dialog
                dialog.hide();
                try{
                    GsonBuilder builder = new GsonBuilder();
                    Gson mGson = builder.create();
                    a = mGson.fromJson(response, ItemObject.class);
                    recyclerViewAdapter = new RecyclerViewAdapter(MainActivity.this,a.results);
                    recyclerView.setAdapter(recyclerViewAdapter);
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    Snackbar.make(findViewById(R.id.swipe_refresh_layout),"Showing "+movieCategory,Snackbar.LENGTH_LONG).show();
                    //Toast.makeText(MainActivity.this, "Show "+movieCategory+" movies", Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("Request Time Out");
                    alert.show();
                }
            }
            //jika ditolak
            @Override
            public void onFailure(int statusCode, Throwable error,String content){
                dialog.hide();
                if (statusCode == 404){
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("404 not Found");
                    alert.show();
                } else if (statusCode == 500){
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("500 Internal Server Error");
                    alert.show();
                } else {
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("No Internet Connection");
                    alert.show();
                }
            }
        });
    }

    //TODO 21: method untuk tampilan menu kanan toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    //TODO 22: method untuk handle ketika menu diklik
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.popular:
                requestJsonObject(0); //Popular
                break;
            case R.id.nowplaying:
                requestJsonObject(1); //Top Rated
                break;
            case R.id.toprated:
                requestJsonObject(2); //Top Rated
                break;
            case R.id.upcoming:
                requestJsonObject(3); //Upcoming
                break;
            case R.id.about:
                //Toast.makeText(getApplicationContext().getApplicationContext(),"About sajah",Toast.LENGTH_SHORT).show();
                Intent aboutIntent = new Intent(this,AboutActivity.class);
                startActivity(aboutIntent);
                break;
            case R.id.action_search:
                handleMenuSearch();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }

    private void handleMenuSearch() {
        ActionBar action = getSupportActionBar(); //get the actionbar

        if(isSearchOpened){ //test if the search is open

            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar

            //hides the keyboard
//            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);

            //add the search icon in the action bar
//            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_open_search));

            //close keyboard
//            InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
////            imm2.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
//            imm2.hideSoftInputFromInputMethod(edtSearch.getWindowToken(),0);
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            isSearchOpened = false;
        } else { //open the search entry

            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title

            edtSearch = (EditText)action.getCustomView().findViewById(R.id.edt_search); //the text editor

            //this is a listener to do a search when the user clicks on search button
            edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch();
                        return true;
                    }
                    return false;
                }
            });


            edtSearch.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);


            //add the close icon
//            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_close_search));

            isSearchOpened = true;
        }
    }

    private void doSearch() {
//        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        handleMenuSearch();
        String searchQuery = edtSearch.getText().toString();
        Toast.makeText(this,"akan lakukan search: "+ searchQuery,Toast.LENGTH_LONG).show();
    }

    // set that user must press back button twice to exit the app
    @Override
    public void onBackPressed() {

        if(isSearchOpened) {
            handleMenuSearch();
            return;
        }

        if (TIME_LIMIT + backPressed > System.currentTimeMillis()) {
            // super.onBackPressed();
            moveTaskToBack(true);
        } else {
            Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show();
        }
        backPressed = System.currentTimeMillis();
    }


}
