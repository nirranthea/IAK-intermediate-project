package com.iak.nirranthea.whatsmovie.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iak.nirranthea.whatsmovie.DetailActivity;
import com.iak.nirranthea.whatsmovie.R;


/**
 * Created by zidi on 10/02/2018.
 */


//TODO: 5 Buat Holder untuk Recycler View extends ViewHolder dan implemen OnClick

public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

//TODO: 6 Deklarasi Variable yang akan digunakan
    public TextView original_title;
    public ImageView img;
    public String overview;
    public String backdrop;
    public String thn;
    public String vote;
    public String id_film;
    public TextView rate;
    public String poster;

    //TODO: 7 set nilai Variable dalam Constructor Holder
    public RecyclerViewHolder(View itemView){
        super(itemView);
        itemView.setOnClickListener(this);
        img = (ImageView) itemView.findViewById(R.id.movie_thumbnail);
        original_title = (TextView)itemView.findViewById(R.id.movie_title);
        rate = (TextView)itemView.findViewById(R.id.movie_star);
    }

    //TODO: 8 set fungsi yang akan dijalankan ketika item Recycler View diKlik
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), DetailActivity.class);

        intent.putExtra("id",id_film);
        intent.putExtra("judul",original_title.getText().toString());
        intent.putExtra("deskripsi",overview);
        intent.putExtra("tahun",thn);
        intent.putExtra("latar",backdrop);
        intent.putExtra("vote",vote);
        intent.putExtra("profpic",poster);

        view.getContext().startActivity(intent);
    }


}
