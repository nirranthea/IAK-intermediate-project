package com.iak.nirranthea.whatsmovie;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zidi on 03/03/2018.
 */

public class TrailerObject {
    @SerializedName("results")
    List<Trailers> results;

    public class Trailers {
        @SerializedName("key")
        public String key;

        @SerializedName("name")
        public String name;

        @SerializedName("site")
        public String site;

        @SerializedName("size")
        public String size;

        @SerializedName("type")
        public String type;

        @SerializedName("id")
        public String id;
    }
}
