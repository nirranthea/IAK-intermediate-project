package com.iak.nirranthea.whatsmovie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iak.nirranthea.whatsmovie.ItemObject;
import com.iak.nirranthea.whatsmovie.R;
import com.iak.nirranthea.whatsmovie.holder.RecyclerViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by zidi on 10/02/2018.
 */

//TODO: 10 buat adapter untuk RecyclerView extends RecyclerView Holder sebagai Adapter
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder>{

    //TODO: 11 deklarasi Variable
    public Context context;
    public List<ItemObject.Results> itemList;

    //TODO: 12 set Constructor dari Variable
    public RecyclerViewAdapter(Context context, List<ItemObject.Results> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

//TODO: 13 set Fungsi yang akan dijalankan ketika Holder mulai dibuat
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.items,null);
        RecyclerViewHolder holder = new RecyclerViewHolder(layoutView);
        return holder;
    }

    //TODO: 14 set fungsi yang akan dijalankan ketika Holder mulai diisi
    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Picasso.with(context).load
                ("https://image.tmdb.org/t/p/w185" + itemList.get(position).poster_path)
                .placeholder(R.drawable.movie_portrait)
                .into(holder.img);

        holder.original_title.setText(itemList.get(position).original_title);
        holder.overview = itemList.get(position).overview;
        holder.thn = itemList.get(position).release_date;
        holder.backdrop = "https://image.tmdb.org/t/p/w780"+itemList.get(position).backdrop_path;
        holder.vote = itemList.get(position).vote_average;
        holder.id_film = itemList.get(position).id;
        holder.rate.setText(itemList.get(position).vote_average + " / 10");
        holder.poster = "https://image.tmdb.org/t/p/w185" + itemList.get(position).poster_path;
    }

    //TODO: 15 Tentukan jumlah item dalam Recycler View
    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}
