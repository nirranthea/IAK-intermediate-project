package com.iak.nirranthea.whatsmovie;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.util.List;


public class DetailActivity extends AppCompatActivity implements View.OnClickListener {
    //TODO 24: Deklarasi Variable
    private TextView txtJudul, txtDeskripsi, txtVote, txtTahun;
    private ImageView imgMovie, imgProfpic;
    private Button btnTrailer;
    private final String API_KEY = "e052653fbd2bce9cacba98622d637e8c";
    private TrailerObject b;
    private AlertDialog.Builder alert;
    private TextView tvTrailer;
    public String movieId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //TODO 25: Deklarasi Komponen
        txtJudul = (TextView) findViewById(R.id.movie_title_dtl);
        txtDeskripsi = (TextView)findViewById(R.id.movie_desc);
        imgMovie = (ImageView)findViewById(R.id.movie_poster);
        txtTahun = (TextView)findViewById(R.id.movie_date);
        txtVote = (TextView)findViewById(R.id.movie_rate);
        imgProfpic = (ImageView)findViewById(R.id.movie_profpic);
        btnTrailer = (Button)findViewById(R.id.btn_trailer);
        btnTrailer.setOnClickListener(this);
        tvTrailer = (TextView)findViewById(R.id.tv_trailer);


        //TODO 26: Ambil Intent
        String latar = getIntent().getStringExtra("latar");
        String jud = getIntent().getStringExtra("judul");
        String des = getIntent().getStringExtra("deskripsi");
        String thn = getIntent().getStringExtra("tahun");
        String vote = getIntent().getStringExtra("vote");
        String poster = getIntent().getStringExtra("profpic");
        this.movieId = getIntent().getStringExtra("id");

        //TODO 27: Inisialisasi Komponen
        Picasso.with(this).load(latar).placeholder(R.drawable.blank_movie).into(imgMovie);
        Picasso.with(this).load(poster).placeholder(R.drawable.blank_movie).into(imgProfpic);
        txtJudul.setText(jud);
        txtDeskripsi.setText(des);
        txtTahun.setText("Release date : "+thn);
        txtVote.setText("Rating : " + vote + " / 10");

        //TODO : tambah tombol back di Toolbar
        getSupportActionBar().setTitle(jud);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    //TODO : Tambah Tombol Back di toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_trailer:
                getTrailer();
                break;

        }
    }

    private void getTrailer() {
        //BUILD URL
        RequestParams params = new RequestParams();
        params.put("api_key",API_KEY);
//        String movieId = id_movie; //ambil id movie detail
        String FullURL = "https://api.themoviedb.org/3/movie/"+movieId+"/videos";

        //Panggil URL melalui Method TrailerParsingGson
        TrailerParsingGson(params,FullURL);
    }

    private void TrailerParsingGson(RequestParams params, String url) {
        //Deklarasi Async
        AsyncHttpClient client = new AsyncHttpClient();

        //Async nembak URL
        client.get(url, params, new AsyncHttpResponseHandler() {

            //jika diterima
            @Override
            public void onSuccess(String response) {
                //sembunyikan kotak dialog
                //dialog.hide();
                try {
                    GsonBuilder builder = new GsonBuilder();
                    Gson mGson = builder.create();
                    b = mGson.fromJson(response, TrailerObject.class);
                    //recyclerViewAdapter = new RecyclerViewAdapter(MainActivity.this,a.results);
                    //recyclerView.setAdapter(recyclerViewAdapter);
//                    if (swipeRefreshLayout.isRefreshing()) {
//                        swipeRefreshLayout.setRefreshing(false);
//                    }
                    //Snackbar.make(findViewById(R.id.swipe_refresh_layout), "Showing " + movieCategory, Snackbar.LENGTH_LONG).show();
                    Toast.makeText(DetailActivity.this, "Show trailer movies", Toast.LENGTH_SHORT).show();
                    List<TrailerObject.Trailers> listTrailer = b.results;
                    tvTrailer.setText(listTrailer.get(0).key);
                } catch (Exception e) {
                    e.printStackTrace();
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("Request Time Out");
                    alert.show();
                }
            }

            //jika ditolak
            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
//                dialog.hide();
                if (statusCode == 404) {
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("404 not Found");
                    alert.show();
                } else if (statusCode == 500) {
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("500 Internal Server Error");
                    alert.show();
                } else {
                    alert.setTitle("Terjadi Kesalahan");
                    alert.setIcon(android.R.drawable.ic_dialog_alert);
                    alert.setMessage("No Internet Connection");
                    alert.show();
                }
            }
        });
    }
}
