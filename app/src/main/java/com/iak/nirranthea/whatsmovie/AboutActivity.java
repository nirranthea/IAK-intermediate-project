package com.iak.nirranthea.whatsmovie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener{

    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_about);

        relativeLayout = (RelativeLayout)findViewById(R.id.about);
        relativeLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
